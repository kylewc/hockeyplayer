/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hockeyplayer;

/**
 *
 * @author kcreamer Create a HockeyMain class which has a player array. There
 * are 12 players on the team. Their numbers are from 1-12. Provide a menu which
 * continues until the enter x.
 *
 * Hockey Tracker A-Add player G-Add game details S-Show players X-Exit
 *
 * When A is chosen a new player should be added. TheRemember their numbers are
 * 1 - 12 and this should be used to determine their index in the array. When G
 * is chosen prompt the user for the player number to determine the correct
 * index and then the addGameDetails from that instance should be invoked. When
 * S is chosen any players that have been added should be shown. s
 */
public class hockeyMain {

    public static void main(String[] args) {
        HockeyPlayer[] players = new HockeyPlayer[12];
        System.out.println("Hockey Tracker");
        boolean stop = false;
        do {
            String select = hockeyplayer.utility.menu("A-Add Player", "G-Add Game Details", "S-Show Players",
                    "X-Exit");
            switch (select) {

                case "a":
                    HockeyPlayer temp = new HockeyPlayer();
                    players[temp.getPlayerNumber() - 1] = temp;
                    break;

                case "g":
                    int number = hockeyplayer.utility.promptForInt("What is the player number?");
                    players[number - 1].addGameDetails();
                    break;

                case "s":
                    for (int i = 0; i < players.length; ++i) {
                        if (players[i] != null) {
                            players[i].toString();
                        }
                    }
                    break;
                case "x":
                    stop = true;
                    break;
                default:
                    System.out.println("Ensure you have entered A, G, S, or X");
                    break;
            }
        } while (stop == false);
    }
}
