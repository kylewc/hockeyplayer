/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hockeyplayer;

/**
 *
 * @author kcreamer
 */
public class HockeyPlayer {

    /**
     * Create a HockeyPlayer class. This class must have an array which holds
     * the goals that the player scored in each game. It must also have an array
     * which holds the opponent team name. There is a ten game season. The class
     * should also have a name and player number attribute. * The default
     * constructor should prompt for the name and player number. The d efault
     * constructor should initialize the number of goals to -1. This is used to
     * indicate that the number of goals has not been specified. * create an
     * instance addGameDetails method which returns void. This method should
     * prompt for the game number, opponent name, and number of goals scored by
     * the player. Array should be updated appropriately.
     *
     * The toString method should return player details as shown in the table
     * below.
     *
     * Table: Player details #9 Will MacLean Game details: Game 1: Morell 2
     * goals Game 3: Souris 0 goals Game 4: Montague 4 goal Total games: 3 Total
     * goals: 4
     */
    private int[] goals = new int[10];
    private String[] teamName = new String[10];
    private String playerName;
    private int playerNumber;
    private int gameNumber;

    public int getPlayerNumber() {
        return playerNumber;
    }

    public HockeyPlayer() {
        playerName = hockeyplayer.utility.promptForString("What is the player name?");
        playerNumber = hockeyplayer.utility.promptForInt("What is the player number?");
        java.util.Arrays.fill(goals, -1);
    }

    public void addGameDetails() {
        gameNumber = hockeyplayer.utility.promptForInt("What is the game number?");
        teamName[gameNumber] = hockeyplayer.utility.promptForString("What is the opposing team's name?");
        goals[gameNumber] = hockeyplayer.utility.promptForInt("How many goals were scored?");
    }

    public String toString() {
        String output;

        for (int counter = 0; counter < goals.length; counter++) {
            if (goals[counter] != -1) {
                System.out.println("\nTable: Player Details#" + playerNumber + " " + playerName + "\nGame details:");
                System.out.println("\nGame " + counter + ": " + teamName[counter] + " " + goals[counter] + " goals");
            }
        }
        int totalGoals = 0;
        int totalGames = 0;
        for (int counter = 0; counter < goals.length; counter++) {
            if (goals[counter] != -1) {
                totalGoals += goals[counter];
                totalGames++;
            }
        }
        if (totalGames != 0) {
            System.out.println("Total Games:  " + totalGames + "\nTotal Goals:  " + totalGoals);
        } else {
            System.out.println("#" + playerNumber + " " + playerName + "\n Total Games: 0 \nTotal Goals: 0");

        }
        return output
    }
}
