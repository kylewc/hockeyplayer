/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hockeyplayer;

import java.util.*;

/**
 *
 * @author kcreamer
 */

public class utility {

    public static String promptForString(String message) {
        Scanner input = new Scanner(System.in);
        System.out.println(message);
        String text = input.nextLine();
        return text;
    }

    public static int promptForInt(String message) {
        Scanner input = new Scanner(System.in);
        System.out.println(message);
        int number = input.nextInt();
        return number;
    }

    public static String menu(String optionA, String optionB, String optionC, String optionD) {
        Scanner input = new Scanner(System.in);
        System.out.println(""
                + optionA + "\n"
                + optionB + "\n"
                + optionC + "\n"
                + optionD);
        String userSelect = input.nextLine();
        String firstLetter = userSelect.substring(0, 1).toLowerCase();

        return firstLetter;
    }

}
